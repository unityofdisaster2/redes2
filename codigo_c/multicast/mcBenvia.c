#include <sys/types.h> //definicion de tipos
#include <sys/socket.h> // llamadas API socket
#include <netinet/in.h> //estructuras de datos
#include <arpa/inet.h> //para socket_addr_in
#include <stdio.h> //printf
#include <stdlib.h> // para atoi
#include <string.h> //para stdlen
#include <unistd.h> //para close

#define MAX_LEN 1024
#define MIN_PORT 1024
#define MAX_PORT 65535

int main(int argc, char const *argv[])
{
    int sock;
    char send_str[MAX_LEN+1];
    struct sockaddr_in mc_addr;
    unsigned int send_len;
    char *mc_addr_str;
    unsigned short mc_port;
    unsigned char mc_ttl=1;//tiempo de vida
    //validamos numero de argumentos
    
    if(argc!=3){
        fprintf(stderr,"Uso %s <Multicast IP><puerto>\n",argv[0]);
        exit(1);
    }
    mc_addr_str = argv[1];
    mc_port = atoi(argv[2]);
    //validamos el numero de puerto
    if((mc_port < MIN_PORT) || (mc_port > MAX_PORT)){
        fprintf(stderr,"Numero de puerto invalido\n");
        exit(1);
    }
    //creamos el socket
    if((sock = socket(PF_INET,SOCK_DGRAM,IPPROTO_UDP))<0){
        perror("Error en el socket()");
        exit(1);
    }
    //asignamos el valor de TTL a 1
    if((setsockopt(sock,IPPROTO_IP,IP_MULTICAST_TTL,(void*)&mc_ttl,sizeof(mc_ttl)))<0){
        perror("Error en el sockopt() de ttl");
        exit(1);
    }

    //construimos la estructura de direccion de multixast
    memset(&mc_addr,0,sizeof(mc_addr));
    mc_addr.sin_family = AF_INET;
    mc_addr.sin_addr.s_addr = inet_addr(mc_addr_str);
    mc_addr.sin_port = htons(mc_port);

    //leemos el texto a enviar
    printf("Comience a escribir (return para enviar ctrl_c para salir)\n");
    //limpiamos el buffer
    memset(send_str,0,sizeof(send_str));
    while(fgets(send_str,MAX_LEN,stdin)){
        send_len = strlen(send_str);
        //enviamos el texto al canal de multicast
        if((sendto(sock,send_str,send_len,0,(struct sockaddr*)&mc_addr,sizeof(mc_addr)))!=send_len){
            perror("sendto() envio un numero incorrecto de bytes");
            exit(1);
        }
        //limpiamos el buffer para el siguiente envio
        memset(send_str,0,sizeof(send_str));
    }
    close(sock);
    exit(0);
}
