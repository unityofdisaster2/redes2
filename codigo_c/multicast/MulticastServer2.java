import java.net.*;
import java.io.*;
/**
 * MulticastServer2
 */
public class MulticastServer2 extends Thread{
    public static final String MCAST_ADDR = "230.0.0.1";
    public static final int MCAST_PORT = 9013;
    public static final int DGRAM_BUF_LEN = 512;
    public void run(){
        String msj = "Hola, soy el servidor de java";
        InetAddress group =  null;
        try {
            group = InetAddress.getByName(MCAST_ADDR);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            System.exit(1);
        }
        for(;;){//lazo infinito del servidor
            try {
                MulticastSocket socket = new MulticastSocket(MCAST_PORT);
                socket.joinGroup(group);
                DatagramPacket packet = new DatagramPacket(msj.getBytes(), msj.length(),group,MCAST_PORT);
                System.out.println("Enviando "+msj+" con un ttl = "+socket.getTimeToLive());

                socket.send(packet);
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(2);
            }
            try {
                Thread.sleep(1000*5);
            } catch (InterruptedException ie) {}
        }
    }
    public static void main(String[] args) {
        try {
            MulticastServer2 ms2 = new MulticastServer2();
            ms2.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}