#include <sys/types.h> //definicion de tipos
#include <sys/socket.h> // llamadas API socket
#include <netinet/in.h> //estructuras de datos
#include <arpa/inet.h> //para socket_addr_in
#include <stdio.h> //printf
#include <stdlib.h> // para atoi
#include <string.h> //para stdlen
#include <unistd.h> //para close

#define MAX_LEN 1024
#define MIN_PORT 1024
#define MAX_PORT 65535

int main(int argc, char const *argv[])
{
    int sock; //descriptor del socket
    int flag_on = 1; //banderas de opcion 
    struct sockaddr_in mc_addr; //Direccion
    char recv_str[MAX_LEN+1];//buffer de lectura
    int recv_len; //longitud de la cadena recibida
    struct ip_mreq mc_req; //Estructura solicitud mc
    char *mc_addr_str; // Direccion IP multicast
    unsigned short mc_port; //puert multicast
    struct sockaddr_in from_addr; //paquete origen
    unsigned int from_len; // longitud direccion arg
    //validacion de parametros
    if(argc != 3){
        fprintf(stderr,"Uso %s <MulticastIP><MulticastPuerto>\n",argv[0]);
        exit(1);
    }
    mc_addr_str = argv[1];//arg1:direccion multicast
    mc_port = atoi(argv[2]); //arg2:puerto multicasts
    //validamos numero de puerto
    if((mc_port < MIN_PORT) || (mc_port > MAX_PORT)){
        fprintf(stderr,"Numero de puerto invalido %d\n",mc_port);
        exit(1);
    }
    //se crea un socket para un canal multicast
    if((sock = socket(PF_INET,SOCK_DGRAM,IPPROTO_UDP)) < 0){
        perror("Error en la creacion del socket\n");
        exit(1);
    }
    //permite la reutilizacion del socket
    if((setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&flag_on,sizeof(flag_on)))<0){
        perror("Error en setsockopt\n");
        exit(1);
    }
    //se construye la estructura sockaddr
    memset(&mc_addr,0,sizeof(mc_addr));
    mc_addr.sin_family = AF_INET;
    mc_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    mc_addr.sin_port = htons(mc_port);
    //liga la direccion con el socket
    if((bind(sock,(struct sockaddr*)&mc_addr,sizeof(mc_addr)))<0){
        perror("Error en bind\n");
        exit(0);
    }
    //estructura para unirse al grupo multicast
    mc_req.imr_multiaddr.s_addr = inet_addr(mc_addr_str);
    mc_req.imr_interface.s_addr = htonl(INADDR_ANY);
    //se une al grupo mediante setsockopt
    if((setsockopt(sock,IPPROTO_IP,IP_ADD_MEMBERSHIP,(void*)&mc_req,sizeof(mc_req)))<0){
        perror("Error en setsockopt(), membership");
        exit(1);
    }
    for(;;){//lazo infinito
        //se limpia el buffer y la estructura de lectura
        memset(recv_str,0,sizeof(recv_str));
        from_len = sizeof(from_addr);
        memset(&from_addr,0,from_len);
        //bloqueo para la recepcion de paquetes
        if((recv_len = recvfrom(sock,recv_str,MAX_LEN,0,(struct sockaddr*)&from_addr,&from_len)) < 0){
            perror("error al recibir paquete\n");
            exit(1);
        }
        //imprimimos lo que se recibio
        printf("se recibieron %d bytes desde %s: ",recv_len,inet_ntoa(from_addr.sin_addr));
        printf("\n%s\n",recv_str);
    }
    //en caso de modificar el lazo para poder salir 
    //modificamos la opcion del socket para salir de 
    if((setsockopt(sock,IPPROTO_IP,IP_DROP_MEMBERSHIP,(void*)&mc_req,sizeof(mc_req)))<0){
        perror("Error en setsockopt drop membership\n");
        exit(1);
    }
    close(sock);
    return 0;
}
