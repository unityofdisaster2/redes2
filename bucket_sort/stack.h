#include <stdio.h>
#include <stdlib.h>


typedef struct s_nodo{
    int valor;
    struct s_nodo *anterior;
}nodo;

typedef nodo *apu_nodo;

typedef struct s_pila{
    apu_nodo aux,actual;
    int size_pila;
    int size_arreglo;
    int *arreglo_pila;
}pila;

void inicializa(pila *p);

void push(pila *p,int valor);

int pop(pila *p);

int tope(pila p);

int tamanio_pila(pila p);

void libera_memoria(pila *p);