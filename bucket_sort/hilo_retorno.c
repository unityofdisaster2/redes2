#include <pthread.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "stack.h"
#include "ordenamiento.h"

//declaracion de funciones

//funcion para generar numeros aleatorios y guardarlos en pila por rangos
void genera_aleatorios(pila *, int *, int);

//funcion para crear rangos de acuerdo a un numero de cubetas
void crea_rangos(int *, int);

//funcion del hilo para ordenar elementos de un arreglo
void *hilo_ordena(void *);



int main(int argc, char **argv)
{
    //se asegura que la semilla de numeros aleatorios siempre sea distinta
    srand(time(NULL));
    if (argc != 2)
    {
        fprintf(stderr, "%s <NUM_BUCKETS>\n", argv[0]);
        exit(-1);
    }
    int num_buckets = atoi(argv[1]);
    //se crean n pilas para guardar los valores de los rangos
    //que le corresponda a cada cubeta
    pila *pilas_contenedoras = malloc(sizeof(pila) * num_buckets);
    int i;
    //se inicializan pilas
    for (i = 0; i < num_buckets; i++)
    {
        inicializa(&pilas_contenedoras[i]);
    }

    //se generan rangos que seran cubiertos por cada cubeta
    int rangos[3500];
    crea_rangos(rangos, num_buckets);
    //se generan aleatorios y se guardan en pilas
    genera_aleatorios(pilas_contenedoras, rangos, num_buckets);

    pthread_t cubetas[num_buckets];

    //se crean y activan hilos en orden arbitrario
    for (i = 0; i < num_buckets; i++)
    {
        pthread_create(&cubetas[i], NULL, &hilo_ordena, &pilas_contenedoras[i]);
    }
    for (i = 0; i < num_buckets; i++)
    {
        pthread_join(cubetas[i], NULL);
    }
    int j,k=0;
    //se recorren arreglos internos de cada estructura personalizada de pila y se guardan
    //sus valores en arreglo que contiene resultado final
    int resultados[3500];
    for (i = 0; i < num_buckets; i++)
    {
        j = 0;
        while (j < pilas_contenedoras[i].size_arreglo)
        {
            //printf("%d\n", pilas_contenedoras[i].arreglo_pila[j]);
            resultados[k] = pilas_contenedoras[i].arreglo_pila[j];
            k++;
            j++;
        }
    }
    printf("------------------------------RESULTADOS------------------------------\n");
    for(i = 0; i < 3500; i++)
    {
        printf("%d\n",resultados[i]);
    }
    printf("------------------------------RESULTADOS------------------------------\n");
    return 0;
}



/*
Funcion para generar numeros aleatorios y guardarlos en un arreglo dentro de 
una estructura de datos
Recibe como argumento: un arreglo de pilas, los rangos que seran cubiertos
por cada pila individual y el numero de cubetas
*/
void genera_aleatorios(pila *arreglo_pilas, int *rangos, int num_buckets)
{
    int i, j, lim = 3500, current_rand;
    printf("--------------------------APARICION INICIAL NUMEROS--------------------------\n");
    for (i = 0; i < lim; i++)
    {
        //se asegura que el numero aleatorio sea de 0 a 999s
        current_rand = rand() % 1000;
        printf("%d\n",current_rand);
        for (j = 0; j < num_buckets; j++)
        {
            
            //se verifican los rangos, en caso de estar dentro de alguno
            //se asigna a la pila que corresponda y se rompe el ciclo 
            if (current_rand <= rangos[j])
            {
                push(&arreglo_pilas[j], current_rand);
                break;
            }
        }
    }
    printf("--------------------------APARICION INICIAL NUMEROS--------------------------\n");
}

/*
Funcion para crear rango dependiendo del numero de cubetas ingresadas 
Recibe como argumento un arreglo y el numero de cubetas
*/
void crea_rangos(int *arreglo, int num_buckets)
{
    //se genera el salto que se dara para generar rangos dividiendo el limite superior entre las cubetas
    int rango = 999 / num_buckets + 1;
    int inf = 0, sup = rango;
    for (int i = 0; i < num_buckets; i++)
    {
        printf("r%d: %d-%d\n", i, inf, sup);
        //se guardan limites superiores en arreglo
        arreglo[i] = sup;
        inf = sup;
        sup += rango;
    }
}


/*
Funcion para ordenar numeros utilizando hilos 
recibe como argumento una que corresponde a un arreglo de hilos
*/
void *hilo_ordena(void *argumento)
{

    printf("inicia hilo\n");
    //se convierte argumento en estructura de pila
    pila *pila_aux = (pila *)argumento;
    //se obtiene el numero total de elementos que tiene la pila
    int total_numeros = tamanio_pila(*pila_aux);
    int i;
    //se crea arreglo y se guardan los elementos de la pila en el
    pila_aux->arreglo_pila = malloc(sizeof(int) * total_numeros);
    for (i = 0; i < total_numeros; i++)
    {
        pila_aux->arreglo_pila[i] = pop(pila_aux);
    }

    mergesort(pila_aux->arreglo_pila, 0, total_numeros - 1);
    //para saber donde termina cada arreglo si se quiere iterar sobre el
    pila_aux->size_arreglo = total_numeros;
    printf("termina hilo\n");
    return 0;
}