#include <stdio.h>
#include <stdlib.h>
#include "ordenamiento.h"

void merge(int *arreglo, int inicio, int nueva_mitad, int mitad)
{
    int rango_i, rango_d, i, j, k;
    //se calculan tamanos de subarreglo izquierdo y derecho
    rango_i = nueva_mitad - inicio + 1;
    rango_d = mitad - nueva_mitad;
    int *izquierdo, *derecho;
    //se crean arreglos temporales para guardar valores de subarreglos izquierdo y derecho
    izquierdo = (int *)malloc(sizeof(int) * rango_i);
    derecho = (int *)malloc(sizeof(int) * rango_d);

    for (i = 0; i < rango_i; i++)
    {
        izquierdo[i] = arreglo[inicio + i];
    }
    for (j = 0; j < rango_d; j++)
    {
        derecho[j] = arreglo[nueva_mitad + 1 + j];
    }
    i = 0;
    j = 0;
    k = inicio;

    /*
    en este ciclo se comparan los valores de los dos subarreglos
    y se ordenan hasta donde sea posible
    */
    while (i < rango_i && j < rango_d)
    {
        if (izquierdo[i] <= derecho[j])
        {
            arreglo[k] = izquierdo[i];
            i++;
        }
        else
        {
            arreglo[k] = derecho[j];
            j++;
        }
        k++;
    }
    /*
    se guardan los valores restantes del subarreglo izquierdo y derecho
    */
    while (i < rango_i)
    {
        arreglo[k] = izquierdo[i];
        i++;
        k++;
    }
    while (j < rango_d)
    {
        arreglo[k] = derecho[j];
        j++;
        k++;
    }
}

void mergesort(int *arreglo, int inicio, int mitad)
{
    int nueva_mitad;
    //se repite de forma recursiva hasta que se llegue al caso base
    //donde solo se tenga un elemento
    if (mitad > inicio)
    {
        nueva_mitad = inicio + (mitad - inicio) / 2;
        //se ordena la primera mitad
        mergesort(arreglo, inicio, nueva_mitad);
        //se ordena la segunda mitad
        mergesort(arreglo, nueva_mitad + 1, mitad);
        //se unen las dos mitades anteriores
        merge(arreglo, inicio, nueva_mitad, mitad);
    }
}
