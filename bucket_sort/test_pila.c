#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
int main(void){
    pila *pila1;
    pila1 = malloc(sizeof(pila)*4);
    inicializa(&pila1[0]);
    push(&pila1[0],1);
    push(&pila1[0],2);
    push(&pila1[0],3);
    push(&pila1[0],4);
    push(&pila1[0],5);
    push(&pila1[0],6);
    push(&pila1[0],7);
    push(&pila1[0],8);
    int aux;
    printf("tope: %d\n",tope(pila1[0]));
    while(1){
        aux = pop(&pila1[0]);
        if(aux != -1){
            printf("el valor de la pila es: %d\n",aux);
        }else {break;}
    }
    libera_memoria(&pila1[0]);
    return 0;
}